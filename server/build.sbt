val dottyVersion = "0.27.0-RC1"

lazy val root = project
  .in(file("."))
  .settings(
    name := "point-salad-server",
    version := "0.1.0",

    scalaVersion := dottyVersion,

    libraryDependencies ++= Seq(
      "org.typelevel" % "cats-core_2.13" % "2.2.0",
      "org.http4s" % "http4s-dsl_2.13" % "0.21.7",
      "org.http4s" % "http4s-blaze-server_2.13" % "0.21.7",
      "com.novocode" % "junit-interface" % "0.11" % "test"
    )
  )
