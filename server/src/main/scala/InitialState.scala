import shared.{Settings, GameState, PlayerId}

def initialState(
  players: Set[PlayerId],
  settings: Settings,
  seed: Seed
): Option[GameState] = None
