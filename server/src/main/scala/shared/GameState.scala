package shared

final case class Player(
  id: PlayerId
)

final case class GameState(
  players: List[Player],
  phase: Phase
)

enum Phase {
  case Beginning(player: PlayerId)
}

