package shared

final case class Settings(
  selectedPlayerCounts: Set[Int]
)