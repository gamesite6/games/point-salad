package shared

opaque type PlayerId = Int
object PlayerId {
  def apply(playerId: Int): PlayerId = playerId
}