opaque type Seed = Long

object Seed {
  def apply(seed: Long): Seed = seed
}
extension (seed: Seed) {
  def toLong: Long = seed
}