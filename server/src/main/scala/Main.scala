import cats.effect.{ExitCode, IO, IOApp}
import shared.{PlayerId, Settings, Action, GameState, Phase}

object Main extends IOApp {

  def run(args: List[String]): IO[ExitCode] =
    for {
      port <- IO(sys.env("PORT"))
      _    <- IO(println(s"Listening on port $port"))
    } yield ExitCode.Success

  def msg = "I was compiled by dotty :)"

  val state = initialState(
    players = Set(PlayerId(1),PlayerId(2), PlayerId(3)),
    settings = Settings(selectedPlayerCounts = Set(3,4,5)),
    seed = Seed(42)
  )

  val nextState = performAction(
    previousState = GameState(players = List(), phase = Phase.Beginning(PlayerId(1))),
    settings = Settings(selectedPlayerCounts = Set(3,4,5)),
    action = Action.Ready,
    seed = Seed(45)
  )
}
