import shared.{GameState, Action, Settings}

def performAction(
  previousState: GameState,
  settings: Settings,
  action: Action,
  seed: Seed
): Option[GameState] = None